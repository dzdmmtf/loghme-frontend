#run command ->sudo docker build . -t react-docker; sudo docker run -p 3000:3000 react-docker
FROM node:alpine as build
COPY package.json yarn.lock ./
RUN yarn && mkdir /react-ui && mv ./node_modules ./react-ui
WORKDIR /react-ui
COPY . .
RUN yarn build
FROM nginx:alpine
#!/bin/sh
COPY ./nginx/nginx.conf /etc/nginx/nginx.conf
RUN rm -rf /usr/share/nginx/html/*
COPY --from=build /react-ui/build /usr/share/nginx/html
EXPOSE 3000 80
CMD ["nginx", "-g", "daemon off;"]
#something
