import React, {Component} from "react";
import axios from 'axios';
import "./RestaurantMenu.css"
import "../common/colors.css";
import "../common/fonts/fonts.css";
import "../common/stylesheet.css";
import {Image, Spinner} from "react-bootstrap";
import Header from "../common/header/Header";
import starLogo from "../common/icons/star.svg"
import minusLogo from "../common/icons/minus.svg"
import plusLogo from "../common/icons/plus.svg"
import FoodModal from "../common/modals/FoodModal";


export default class RestaurantMenu extends Component {
    constructor(props) {
        super(props);
        this.state = {
            foods: {},
            orders: {},
            ordersJsonArray: [],
            statusCode: 0,
            loadFood: false,
            modalFood: {},
            modalRestaurantName: "",
            orderIsLoaded: false,
            foodIsLoaded: false,
            hasJWTError:false
        };
        this.handleClose = this.handleClose.bind(this);
    }

    componentDidMount() {
        axios.get("http://185.166.105.6:31150/LoghmeServlet/cart",
            {headers: {Authorization: `Bearer ${localStorage.getItem("token")}`}})
            .then(response => {
            this.setState({orders: response.data, orderIsLoaded: true});
        }).catch(error => {
            this.jwtErrorHandling(error)
        });
        axios.get("http://185.166.105.6:31150/LoghmeServlet/restaurants/" + this.props.match.params.restaurantId,
            {headers: {Authorization: `Bearer ${localStorage.getItem("token")}`}})
            .then(response => {
            this.setState({foods: response.data, statusCode: response.data.statusCode, foodIsLoaded: true});
        }).catch(error => {
            this.jwtErrorHandling(error)
        });
    }

    jwtErrorHandling(error){
        this.setState({hasJWTError:true})
    }

    increaseItem(orderedFoodId, restaurantId) {
        axios.post("http://185.166.105.6:31150/LoghmeServlet/cart/single", null,
            {
                params: {orderedFoodId, restaurantId},
                headers: {Authorization: `Bearer ${localStorage.getItem("token")}`}})
            .then(response => {
                this.setState({orders: response.data})
            })
            .catch(error => {
                this.jwtErrorHandling(error)
            })
    }

    decreaseItem(orderedFoodId) {
        axios.delete("http://185.166.105.6:31150/LoghmeServlet/cart",
            {
                data: {orderedFoodId},
                headers: {Authorization: `Bearer ${localStorage.getItem("token")}`}})
            .then(
                response => {
                    this.setState({orders: response.data})
                }
            )
            .catch(error => {
                this.jwtErrorHandling(error)
            })
    }

    addToCart(foodId, restaurantId) {
        const path = "/cart/addToCart/" + foodId + "/" + restaurantId;
        this.props.history.push({pathname: path, foodId: foodId, restaurantId: restaurantId});
    }

    finalizeOrder(ordersNum, restaurantId) {
        const path = "/cart/finalizeOrder/" + ordersNum;
        this.props.history.push({pathname: path, ordersNum: ordersNum, restaurantId: restaurantId});
    }

    UnderHeader = () => (
        <div className="row">
            <div className="restaurant-menu-under-header pastel-red-BG-color"> </div>
        </div>
    );

    MinusItem = (props) => (
        <input type="image"
               src={minusLogo}
               alt=""
               width="15"
               height="15"
               onClick={() => this.decreaseItem(props.orderedItem.orderedFoodId)}
        />
    );

    PlusItem = (props) => (
        <input type="image"
               src={plusLogo}
               alt=""
               width="15"
               height="15"
               onClick={() => this.increaseItem(props.orderedItem.orderedFoodId, this.state.orders.restaurantId)}
        />
    );

    Orders = (props) => (
        <div className="col-12 restaurant-menu-my-col-orders">
            <div className="restaurant-menu-item horizontal-centered">
                <div className="row">
                    <div className="col-4 restaurant-menu-amount-div">
                        <div className="row">
                            <div className="span-4 restaurant-menu-minus">
                                <this.MinusItem orderedItem={props.data}/>
                            </div>
                            <div className="span-4 restaurant-menu-number font-thin"><p>{props.data.amount}</p></div>
                            <div className="span-4 restaurant-menu-plus">
                                <this.PlusItem orderedItem={props.data}/>
                            </div>
                        </div>
                    </div>
                    <div className="col-8 restaurant-menu-item-name font-thin">
                        <p>{props.data.name}</p>
                    </div>
                </div>
                <div className="restaurant-menu-item-price font-thin">
                    <p>{props.data.price} تومان</p>
                </div>
            </div>
        </div>
    );

    Foods = props => (
        <div className="col-4 restaurant-menu-myCol">
            <div className="restaurant-menu-food-body horizontal-centered white-BG-color border-radius7 shadow col">
                <input type="image"
                       className="horizontal-centered restaurant-menu-food-pic-size restaurant-menu-food-image"
                       src={props.data.image} alt=""
                       onClick={() => this.setState({
                           loadFood: true,
                           modalFood: props.data,
                           modalRestaurantName: props.restaurantName
                       })}/>
                <div className="restaurant-menu-food-name-rank col row">
                    <div className="col-2 row">
                        <Image className="restaurant-menu-star" src={starLogo} width="20" height="20" alt=""/>
                    </div>
                    <div className="col-2 row">
                        <p className="restaurant-menu-rank font-thin">{props.data.popularity}</p>
                    </div>
                    <div className="col-7 row">
                        <p className="restaurant-menu-food-name font-thin font-size8">{props.data.name}</p>
                    </div>
                </div>
                <div className="restaurant-menu-price horizontal-centered">
                    <p>{props.data.price} تومان</p>
                </div>
                <div>
                    <button
                        className="restaurant-menu-food-button button horizontal-centered maize-BG-color border-radius7 black-color font"
                        type="button" onClick={() => this.addToCart(props.data.foodId, props.restaurantId)}>افزودن
                        به سبد خرید
                    </button>
                </div>
            </div>
        </div>
    );

    handleClose() {
        this.setState({loadFood: false});
    }

    render() {
        let body;
        // setInterval(this.componentDidMount(), 5000);
        if(this.state.hasJWTError){
            this.props.history.push({pathname: "/login"});
        }
        if (this.state.statusCode === "FORBIDDEN") {
            body =
                <div className="horizontal-centered restaurant-menu-invalid-status-code">با عرض پوزش، این رستوران در محل
                    سرویس دهی شما نمی‌باشد.لطفا منو رستوران دیگری را مشاهده نمایید</div>
        } else if (this.state.statusCode === "NOT_FOUND") {
            body =
                <div className="horizontal-centered restaurant-menu-invalid-status-code">رستوران مورد نظر یافت نشد</div>
        } else {
            body =
                <div>
                    {this.state.foodIsLoaded &&
                    <Image className="restaurant-menu-logo border-radius15 horizontal-centered"
                           src={this.state.foods.logo} width="200" height="200"/>}
                    {!this.state.foodIsLoaded && <Spinner className="spinner-location" animation="border" role="status">
                        <span className="sr-only">Loading...</span>
                    </Spinner>}
                    <div className="row mint-cream-BG-color">
                        <div className="restaurant-menu-restaurant-name mint-cream-BG-color">
                            < p className="horizontal-centered  black-color font-bold">{this.state.foods.name}</p>
                        </div>
                    </div>
                    <div className="restaurant-menu-restaurant-container">
                        <div className="row">
                            <div className="restaurant-menu-menu col-8">
                                <div className="restaurant-menu-menu-text horizontal-centered">
                                    <p className="horizontal-centered font-bold text-line">منوی غذا</p>
                                </div>
                                {this.state.foodIsLoaded && <div className="restaurant-menu-menu-item-row row">
                                    {this.state.foods.menu.map((food, index) => (
                                        <this.Foods data={food} restaurantName={this.state.foods.name}
                                                    restaurantId={this.state.foods.id} key={index}/>))}
                                </div>}
                                {!this.state.foodIsLoaded &&
                                <Spinner className="spinner-location-restaurant-menu" animation="border" role="status">
                                    <span className="sr-only">Loading...</span>
                                </Spinner>}

                                <div className="restaurant-menu-vertical-dashed-line"> </div>
                            </div>
                            <div className="restaurant-menu-cart col-4 mint-cream-BG-color">
                                <div
                                    className="restaurant-menu-cart-box-1 horizontal-centered white-BG-color border-radius7 shadow">
                                    <div className="restaurant-menu-cart-name">
                                        <p className="horizontal-centered font-bold font-size18 black-color text-line">سبد
                                            خرید</p>
                                    </div>
                                    {this.state.orderIsLoaded &&
                                    <div className="row restaurant-menu-cart-items horizontal-centered">
                                        {(this.state.orders.ordersNum !== 0) ? this.state.orders.order.map((food, index) => (
                                            <this.Orders data={food} key={index}/>)) : <div> </div>}
                                    </div>}
                                    {!this.state.orderIsLoaded &&
                                    <Spinner className="spinner-location-restaurant-menu" animation="border"
                                             role="status">
                                        <span className="sr-only">Loading...</span>
                                    </Spinner>}
                                    <div className="restaurant-menu-total-price horizontal-centered font-bold">
                                        <p>جمع کل : {this.state.orders.totalPrice} تومان</p>
                                    </div>
                                    <div>
                                        <button
                                            className="restaurant-menu-cart-button button horizontal-centered border-radius7 font font-bold medium-turquoise-BG-color"
                                            type="button"
                                            onClick={() => this.finalizeOrder(this.state.orders.ordersNum, this.state.orders.restaurantId)}>تایید
                                            نهایی
                                        </button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
        }

        return (
            <div>
                <Header/>
                <FoodModal displayModal={this.state.loadFood} closeModal={this.handleClose} data={this}/>
                <this.UnderHeader/>
                {body}
            </div>
        )
    }
}
