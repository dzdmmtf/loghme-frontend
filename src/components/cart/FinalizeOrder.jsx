import "./FinalizeOrder.css"
import "../common/colors.css";
import "../common/fonts/fonts.css";
import "../common/stylesheet.css";
import React, {Component} from "react";
import axios from "axios";
import Header from "../common/header/Header";

const UnderHeader = () => (
    <div className="row">
        <div className="under-header pastel-red-BG-color"></div>
    </div>
);

export default class FinalizeOrder extends Component {
    constructor(props) {
        super(props);
        this.state = {
            ordersNum: "-1",
            insufficientCredit: false,
            statusCode: "",
            hasJWTError: false
        };
    }

    componentDidMount() {
        if (String(this.props.match.params.ordersNum) !== "0") {
            axios.post("http://185.166.105.6:31150/LoghmeServlet/orders", {}, {headers: {Authorization: `Bearer ${localStorage.getItem("token")}`}})
                .then(response => {
                if(response.data.statusCode && response.data.statusCode === "FORBIDDEN") {
                    this.setState({insufficientCredit: true});
                    this.setState({statusCode: "FORBIDDEN"});
                }
                else {
                    this.setState({ordersNum: String(this.props.match.params.ordersNum)})
                    axios.get("http://185.166.105.6:31150/LoghmeServlet/delivery", {headers: {Authorization: `Bearer ${localStorage.getItem("token")}`}})
                        .then(()=>{})
                        .catch(error => {
                            this.jwtErrorHandling(error);
                        });
                }
            }).catch(error => {
                this.jwtErrorHandling(error)
            });
        }
    }

    jwtErrorHandling(error){
        console.log(error.response)
        this.setState({hasJWTError:true})
    }

    render() {
        let body;
        if(this.state.hasJWTError){
            this.props.history.push({pathname: "/login"});
        }
        if (String(this.props.match.params.ordersNum) === "0") {
            body =
                <div className="horizontal-centered invalid-status-code">کاربر گرامی،سبد خرید شما خالی می‌باشد.با انتخاب
                    غذا سفارش خود را به ثبت رسانید</div>
        } else if(this.state.statusCode === "FORBIDDEN"){
            body = <div className="horizontal-centered invalid-status-code">کاربر گرامی شما قادر به خرید نیستید، لطفا حساب خود را شارژ کنید.</div>
        }
        else{
            body = <div className="horizontal-centered invalid-status-code">کاربر گرامی،سفارش شما با موفقیت به ثبت
                رسید.ممنون که لقمه را انتخاب نمودید:)</div>
        }
        return (
            <div>
                <Header/>
                <UnderHeader/>
                {body}
            </div>
        )
    }
}