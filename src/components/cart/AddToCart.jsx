import React, {Component} from "react";
import "./AddToCart.css"
import "../common/colors.css";
import "../common/fonts/fonts.css";
import "../common/stylesheet.css";
import Header from "../common/header/Header";
import axios from "axios";

const UnderHeader = () => (
    <div className= "row">
        <div className="under-header pastel-red-BG-color"></div>
    </div>
);

export default class AddToCart extends Component {
    constructor(props) {
        super(props);
        this.state = {
            foodName: "",
            restaurantName: "",
            statusCode:0,
            hasJWTError: false
        };
    }

    componentDidMount(props){
        const foodId = String(this.props.match.params.foodId)
        const restaurantId = String(this.props.match.params.restaurantId)
        const orderAmount = "1";
        axios.post("http://185.166.105.6:31150/LoghmeServlet/cart", null,{params:{foodId, restaurantId, orderAmount}, headers: {Authorization: `Bearer ${localStorage.getItem("token")}`}})
        .then(response=>{
            this.setState({statusCode:response.data.statusCode});
        })
            .catch(error => {
                this.jwtErrorHandling(error);
            });
    }

    jwtErrorHandling(error){
        console.log(error.response)
        this.setState({hasJWTError:true})
    }

    render() {
        let body;
        if(this.state.hasJWTError){
            this.props.history.push({pathname: "/login"});
        }
        if(this.state.statusCode === "BAD_REQUEST"){
            body = <div className="horizontal-centered invalid-status-code">درخواست غیرقابل قبول است</div>
        }
        else if(this.state.statusCode === "FORBIDDEN"){
            body = <div className="horizontal-centered invalid-status-code">با عرض پوزش، سفارش شما قابل پردازش نیست.شما از رستوران دیگری سفارش خود را ثبت کرده‌اید.</div>
        }
        else if(this.state.statusCode === "NOT_FOUND"){
            body = <div className="horizontal-centered invalid-status-code">رستوران/غذا مورد نظر یافت نشد</div>
        }
        else if(this.state.statusCode === "OK") {
            body = <div className="horizontal-centered accepted-order">سفارش با موفقیت ثبت شد</div>
        }
        return (
            <div>
                <Header/>
                <UnderHeader/>
                {body}
            </div>
        )
    }
}