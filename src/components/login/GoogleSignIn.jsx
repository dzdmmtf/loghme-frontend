import React, {Component} from "react";
import axios from "axios";
import {withRouter} from "react-router-dom";

const GOOGLE_BUTTON_ID = "google-sign-in-button";

class GoogleSignIn extends Component {
    constructor(props) {
        super(props);
        this.state = {
            loadHome: false,
            loadSignup: false
        };
        this.onSuccess = this.onSuccess.bind(this);
    }

    componentDidMount() {
        if (window.gapi) {
            window.gapi.signin2.render(GOOGLE_BUTTON_ID, {
                width: 150,
                height: 50,
                onsuccess: this.onSuccess
            });
            window.onbeforeunload = function(e){
                window.gapi.auth2.getAuthInstance().signOut().then(window.gapi.auth2.getAuthInstance().disconnect());
            };
        }
    }

    onSuccess(googleUser) {
        const idToken = googleUser.getAuthResponse().id_token;
        axios.post("http://185.166.105.6:31150/LoghmeServlet/login/google", {idToken}).then(response => {
            if (response.data.status === "OK") {
                localStorage.setItem("token", response.data.jwt);
                this.setState({loadHome: true});
            } else if (response.data.status === "NOT_FOUND") {
                alert("چنین حسابی در سیستم وجود ندارد.برای ورود به سامانه ثبت نام نمایید");
                this.setState({loadSignup: true});
            }
        })
    }

    render() {
        if(this.state.loadHome)
            this.props.history.push({pathname: "/"});
        else if(this.state.loadSignup)
            this.props.history.push({pathname: "/signup"});

        return <div id={GOOGLE_BUTTON_ID}/>;
    }
}

export default withRouter(GoogleSignIn);