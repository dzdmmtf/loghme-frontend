import "./login.css"
import "../common/colors.css";
import "../common/fonts/fonts.css";
import "../common/stylesheet.css";
import React, {Component} from "react";
import {Image} from "react-bootstrap";
import registerLogo from "../common/icons/register.svg";
import Footer from "../common/footer/Footer";
import axios from "axios";
import GoogleSignIn from "./GoogleSignIn";

export default class Login extends Component {
    constructor(props) {
        super(props);
        this.state = {
            goToHomePage: false,
            showAlert: false,
            alertMsg: "",
            alertVariant: ""
        };
    }

    componentDidMount() {
        axios.get('http://185.166.105.6:31150/LoghmeServlet/jwtChecker',
            {headers: {Authorization: `Bearer ${localStorage.getItem("token")}`}}
        )
            .then(response => {
            }).catch(error => {
            if(error.response && error.response.status === 400)
                this.setState({goToHomePage:true})
        })
    }

    getUserInfo() {
        const email = document.getElementById("email").value;
        const password = document.getElementById("password").value;
        if (email === "" || password === "") {
            alert("کاربر گرامی، اطلاعات وارد شده ناقص می‌باشد.")
        } else {
            axios.post("http://185.166.105.6:31150/LoghmeServlet/login", {email, password},
                {headers: {Authorization: `Bearer ${localStorage.getItem("token")}`}})
                .then(response => {
                if (response.data.status === "OK") {
                    alert("ورود با موفقیت انجام شد")
                    console.log("JWT::: " + response.data.jwt);
                    localStorage.setItem("token", response.data.jwt);
                    this.props.history.push({pathname: "/"});
                } else if (response.data.status === "NOT_FOUND") {
                    alert("چنین حسابی در سیستم وجود ندارد.برای ورود به سامانه ثبت نام نمایید");
                    this.props.history.push({pathname: "signup"});
                } else if (response.data.status === "FORBIDDEN") {
                    alert("رمز عبور اشتباه است")
                }
            })
        }
    }

    SignInBox = () => (
        <div className="main-container mintCreamBGColor border-radius7 horizontal-centered">
            <div className="main-container-header gainsboro-BG-color">
                <Image src={registerLogo} width="27" height="27" alt=""/>
                <p className="font font-size18 horizontal-centered">ورود</p>
            </div>
            <div className="main-container-body">
                <input className="email horizontal-centered font text-line" type="text" id="email" name="email"
                       placeholder="ایمیل"/><br/><br/>
                <input className="password horizontal-centered font text-line" type="password" id="password"
                       name="password" placeholder="رمزعبور"/><br/><br/>
                <div className="login-area">
                    <div>
                        <button
                            className="button font font-size16 trolley-grey-color gainsboro-BG-color"
                            type="button"
                            onClick={() => this.getUserInfo()}>ورود
                        </button>
                    </div>
                    <GoogleSignIn/>
                </div>
            </div>
        </div>
    );

    render() {
        if(this.state.goToHomePage){
            this.props.history.push({pathname: "/"});
        }
        return (
            <div className="sign-in">
                <this.SignInBox/>
                <Footer/>

            </div>
        )
    }
}
