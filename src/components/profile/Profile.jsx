import React, {Component} from 'react';
import Header from "../common/header/Header";
import phoneImage from "../common/icons/008-phone.png";
import mailImage from "../common/icons/007-mail.png";
import accountImage from "../common/icons/004-account.png";
import cardImage from "../common/icons/006-card.png";
import './Profile.css';
import '../common/colors.css';
import axios from 'axios';
import Button from "react-bootstrap/Button";
import Footer from "../common/footer/Footer";
import Modal from "../common/modals/OrderModal";
import Spinner from "react-bootstrap/Spinner";


const ProfileHeader = props => (
    <div className="container-fluid fixed-margin">
        <div className="profile-bar row">
            <div className="col-sm-6">
                <div className="profile-items flex-row-reverse">
                    <img className="profile-icon font" src={phoneImage} width="20" height="20" alt=""/>
                    <p className="profile-content font">{props.data.state.profile.phone}</p>
                </div>
                <div className="profile-items flex-row-reverse">
                    <img className="profile-icon" src={mailImage} width="20" height="20" alt=""/>
                    <p className="profile-content font">{props.data.state.profile.email}</p>
                </div>
                <div className="profile-items flex-row-reverse">
                    <img className="profile-icon font" src={cardImage} width="20" height="20" alt=""/>
                    <p className="profile-content font">  {props.data.state.profile.credit} تومان </p>
                </div>
            </div>
            <div className="col-sm-6 profile-account flex-row-reverse">
                <img className="profile-icon" src={accountImage} width="50" height="50" alt=""/>
                <p className="font-bold font-xx-large">{props.data.state.profile.fullname}</p>
            </div>
        </div>
    </div>
);


const ProfileTabs = props => (
    <div className="profile-tabs flex-row-reverse">
        <div className="row">
            <Button className={"charge-tab tab-size tab-shadow " + (props.data.state.loadOrders ? "white-BG-color" : "pastel-red-BG-color")}
                    onClick={e => props.data.setState({loadOrders: false})}>
                <p className={"font-bold tab-text " + (props.data.state.loadOrders ? "black-color" : "white-color")}>افزایش ‌اعتبار</p>
            </Button>

            <Button className={"orders-tab tab-size tab-shadow " + (props.data.state.loadOrders ? "pastel-red-BG-color" : "white-BG-color")}
                    onClick={e => {props.data.setState({loadOrders: true}); props.data.getOrders()}}>
                <p className={"font-bold tab-text black-color " + (props.data.state.loadOrders ? "white-color" : "black-color")}>سفارش‌ها</p>
            </Button>
        </div>
    </div>
);


const Orders = props => {
    const status = (props.status === "old") ? "مشاهده‌ فاکتور" : (props.status === "found") ? "پیک در مسیر" : "در جست‌وجوی پیک";
    const statusColor = (props.status === "old") ? "maize-BG-color" : (props.status === "found") ?
        "in-way-BG-color" : "medium-turquoise-BG-color";
    const number = (props.status === "old") ? (props.index + 1) : (props.data.state.orders.oldOrders.length + 1);
    return (
        <div className="order-items">
            <div className="order-id">
                <p className="font order-id-text">{number}</p>
            </div>
            <div className="line-div">
                <div className="lines"> </div>
            </div>
            <div className="order-restaurant-name-div">
                <p className="font order-restaurant-name">{props.order.restaurantName}</p>
            </div>
            <div className="line-div">
                <div className="lines"> </div>
            </div>
            <div className={"delivery-status tab-shadow " + statusColor}
                 onClick={() => props.data.showOrderDetail(props.order)}>
                <p className="font delivery-status-text">{status}</p>
            </div>
        </div>
    )
};


const ChargeAccount = props => (
    <div className="profile-border credit-box-height">
        <div className="row">
            <div className="col-sm-6">
                <button
                    className="charge-account-button charge-account-items white-color font-bold font-size20"
                    onClick={props.data.chargeAccount}>افزایش
                </button>
            </div>
            <div className="col-sm-6">
                <form>
                    <input className="charge-account-input charge-account-items font font-size20" type="text"
                           name="credit" placeholder="میزان‌ افزایش‌ اعتبار   " value={props.data.state.credit}
                           onChange={props.data.handleCreditChange}/>
                </form>
            </div>
        </div>
    </div>
);


export default class Profile extends Component {

    constructor(props) {
        super(props);
        this.state = {
            profile: {},
            loadOrders: false,
            credit: "",
            cart: [],
            orders: {},
            loadOrderDetail: false,
            modalOrder: [],
            orderIsLoaded: false,
            hasJWTError: false
        };
        
        this.handleCreditChange = this.handleCreditChange.bind(this);
        this.chargeAccount = this.chargeAccount.bind(this);
        this.getOrders = this.getOrders.bind(this);
        this.showOrderDetail = this.showOrderDetail.bind(this);
        this.handleClose = this.handleClose.bind(this);
    }

    componentDidMount() {
        axios.get('http://185.166.105.6:31150/LoghmeServlet/profile',
            {headers: {Authorization: `Bearer ${localStorage.getItem("token")}`}}).then(response => {
            this.setState({profile: response.data});
        }).catch(error => {
            this.jwtErrorHandling(error)
        })
    }

    jwtErrorHandling(error){
        console.log(error.response);
        this.setState({hasJWTError:true})
    }
    
    handleCreditChange(event) {
        this.setState({credit: event.target.value})
    }

    chargeAccount(event) {
        axios.post('http://185.166.105.6:31150/LoghmeServlet/account', {credit: this.state.credit},
            {headers: {Authorization: `Bearer ${localStorage.getItem("token")}`}})
            .then(response => {
            this.setState(prevState => ({
                profile: {
                    ...prevState.profile,
                    credit: parseInt(this.state.credit) + this.state.profile.credit
                },
                credit: ""
            }))
        }).catch(error => {
            this.jwtErrorHandling(error)
        })
    }

    getOrders() {
        axios.get('http://185.166.105.6:31150/LoghmeServlet/orders',
            {headers: {Authorization: `Bearer ${localStorage.getItem("token")}`}})
            .then(response => {
            this.setState({orders: response.data, orderIsLoaded: true});
            console.log('orders: ',response.data)
        })
            .catch(error => {
                this.jwtErrorHandling(error)
        })
    }

    showOrderDetail(order) {
        this.setState({
            loadOrderDetail: true,
            modalOrder: order,
        })
    }

    handleClose() {
        this.setState({loadOrderDetail: false});
    }


    render() {
        if(this.state.hasJWTError){
            this.props.history.push({pathname: "/login"});
        }
        return (
            <div>
                <Header isHomePage={false}/>
                <ProfileHeader data={this}/>
                <ProfileTabs data={this}/>

                {this.state.loadOrders && this.state.orderIsLoaded && <div className="profile-border orders-box">
                    {this.state.orders.oldOrders && this.state.orders.oldOrders.map((oldOrder, index) => (
                          <Orders order={oldOrder} status={"old"} index={index} key={index} data={this} />
                    ))}
                    {this.state.orders.currentOrders.restaurantName && <Orders order={this.state.orders.currentOrders}
                           status={this.state.orders.currentOrders.status} data={this} />}
                </div> }
                {this.state.loadOrders && !this.state.orderIsLoaded && <Spinner className="spinner-location" animation="border" role="status">
                    <span className="sr-only">Loading...</span>
                </Spinner>}

                {this.state.loadOrderDetail && <Modal displayModal={this.state.loadOrderDetail} closeModal={this.handleClose} data={this}/>}

                {!this.state.loadOrders && <ChargeAccount data={this}/>}
                <Footer/>
            </div>
        )
    }
}