import React,{Component} from "react";
import Logo from "../pics/LOGO.png";
import smartCart from "../icons/005-smart-cart.png";
import Navbar from "react-bootstrap/Navbar";
import Nav from "react-bootstrap/Nav";
import '../colors.css';
import './Header.css'
import axios from "axios";
import CartModal from "../modals/CartModal";
import {withRouter} from "react-router-dom";

class Header extends Component {

    constructor(props) {
        super(props);
        this.state = {
            data: {},
            loadCart: false,
            hasJWTError: false
        };

        this.handleClose = this.handleClose.bind(this);
        this.logout = this.logout.bind(this);
    }

    componentDidMount() {
        axios.get("http://185.166.105.6:31150/LoghmeServlet/cart/num",
            {headers: {Authorization: `Bearer ${localStorage.getItem("token")}`}}
            )
            .then(response => {
                console.log(response)
                this.setState({data:response.data})
        })
            .catch(error => {
                this.jwtErrorHandling(error)
                alert("hi" + error.response.data)
            });
    }

    handleClose() {
        this.setState({loadCart: false});
    }

    logout() {
        axios.get("http://185.166.105.6:31150/LoghmeServlet/logout",
            {headers: {Authorization: `Bearer ${localStorage.getItem("token")}`}}).then(response => {
            localStorage.clear();
        }).catch(error => {
            console.log(error);
        })
    }

    jwtErrorHandling(error){
        console.log(error.response)
        if(error.response && error.response.status === 401)
            alert("کاربر گرامی! برای استفاده از سامانه می‌بایست احراز هویت شوید.بدین منظور درصورت داشتن حساب کاربری وارد سامانه شوید در غیر این صورت ثبت نام کنید.")
        else if(error.response && error.response.status === 403) {
            alert("اشکالی در فرآیند رخ داده است.لطفا مجددا وارد سامانه شوید.")
            localStorage.clear();
        }
        this.setState({hasJWTError:true})
    }

    render() {
        // setInterval(this.componentDidMount(), 1000);
        if(this.state.hasJWTError){
            this.props.history.push({pathname: "/login"});
        }
        return (
            <div className="white-BG-color fixed-top navbar-position">
                <CartModal displayModal={this.state.loadCart} closeModal={this.handleClose} data={this}/>
                <Navbar>
                    <Nav.Link className="red-color" href="/login" onClick={() => this.logout()}>خروج</Nav.Link>
                    <Nav.Link className="black-color" href="/profile">حساب کاربری</Nav.Link>
                    <Navbar.Brand href="#home">
                        <img onClick={() => this.setState({loadCart: true})}
                             src={smartCart}
                             width="30"
                             height="30"
                             className="d-inline-block align-top"
                             alt=""
                        />
                    </Navbar.Brand>
                    <div className="orderNum medium-turquoise-BG-color">
                        <p className="cart-number-pos white-color eng-font-size16">
                            {this.state.data.amount}
                        </p>
                    </div>
                    {!this.props.isHomePage &&
                    <Navbar.Collapse className="justify-content-end">
                        <Navbar.Brand href="/">
                            <img
                                src={Logo}
                                width="45"
                                height="45"
                                className="d-inline-block align-top"
                                alt=""
                            />
                        </Navbar.Brand>
                    </Navbar.Collapse>
                    }
                </Navbar>
            </div>
        )
    }
}
export default withRouter(Header)