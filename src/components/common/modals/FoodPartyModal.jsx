import React, {Component} from 'react';
import './FoodPartyModal.css';
import star from "../icons/003-star.png";
import {Button} from "react-bootstrap";
import axios from 'axios';
import minusLogo from "../icons/minus.svg";
import plusLogo from "../icons/plus.svg";


export default class FoodPartyModal extends Component {
    constructor(props) {
        super(props);
        this.state = {
            orderAmount: 0,
            statusCode: "",
            loadPopOver: false,
            show: false,
            hasJWTError: false
        };

        this.closeModal = this.closeModal.bind(this);
    }

    closeModal(e) {
        e.stopPropagation();
        this.props.closeModal()
    }

    jwtErrorHandling(error){
        if(error.response && error.response.status === 401)
            alert("کاربر گرامی! برای استفاده از سامانه می‌بایست احراز هویت شوید.بدین منظور درصورت داشتن حساب کاربری وارد سامانه شوید در غیر این صورت ثبت نام کنید.")
        else if(error.response && error.response.status === 403) {
            alert("اشکالی در فرآیند رخ داده است.لطفا مجددا وارد سامانه شوید.")
            localStorage.clear();
        }
        this.setState({hasJWTError:true})
    }

    addToCart(restaurantId, orderAmount) {
        axios.post("http://185.166.105.6:31150/LoghmeServlet/foodParty", null, {
            params: {restaurantId, orderAmount},
            headers: {Authorization: `Bearer ${localStorage.getItem("token")}`}})
            .then(response => {
            this.setState({statusCode: response.data.statusCode});
            if (this.state.statusCode === "OK") {
                this.setState(prevState => ({
                    orderAmount: 0
                }))
            } else if (this.state.statusCode === "FORBIDDEN") {
                this.setState({
                    loadPopOver: true,
                })
            }
        })
            .catch(error => {
                this.jwtErrorHandling(error)
            });
    }

    increaseItem(count){
        this.setState(prevState => ({
            orderAmount: (count - this.state.orderAmount-1 >=0) ? (this.state.orderAmount + 1) : this.state.orderAmount
        }))
    }

    decreaseItem() {
        this.setState(prevState => ({
            orderAmount: (this.state.orderAmount - 1 >= 0)?(this.state.orderAmount - 1):(0)
        }))
    }

    render() {
        if(this.state.hasJWTError){
            this.props.history.push({pathname: "/login"});
        }
        const divStyle = {
            display: this.props.displayModal ? 'block' : 'none'
        };
        return (
            <div className="modal" onClick={this.closeModal} style={divStyle}>
                <div className="modal-content" onClick={e => e.stopPropagation()}>
                    <div className="restaurant-name-modal">
                        <p className="font-thin text-center font-x-large">{this.props.data.state.restaurantName}</p>
                    </div>

                    <div className="flex-container flex-row food-info-modal">
                        <div className="food-width">
                            <div className="flex-view">
                                <p className="font food-name-modal">{this.props.data.state.food.name}</p>
                                <img className="star-image-modal" src={star} alt=""/>
                                <p className="font popularity-text-modal">{this.props.data.state.food.popularity}</p>
                            </div>

                            <p className="font description-text">{this.props.data.state.food.description}</p>
                            <div className="price-part">
                                <div className="unit-modal">
                                    <p className="font price-text-modal">تومان</p>
                                </div>
                                <div className="current-price-modal">
                                    <p className="font price-text-modal">{this.props.data.state.food.price}</p>
                                </div>
                                <div className="prev-price-modal">
                                    <p className="font price-text-modal">{this.props.data.state.food.oldPrice}</p>
                                </div>
                            </div>
                        </div>

                        <div className="image-width">
                            <img className="food-image-modal" src={this.props.data.state.food.image} alt=""/>
                        </div>
                    </div>

                    <div className="purchase-modal">

                        <Button
                            className={"purchase-button-modal " + ((parseInt(this.props.data.state.food.count) > 0) ? "medium-turquoise-BG-color" : "grey-BG-color")}
                            onClick={() => {
                                this.addToCart(this.props.data.state.restaurantId, this.state.orderAmount);
                            }}>
                            <p className="font white-color text-center">افزودن به سبد خرید</p>
                        </Button>

                        <input type="image"
                               src={minusLogo}
                               className="count-button"
                               alt=""
                               width="20"
                               height="20"
                               onClick={() => this.decreaseItem()}
                        />
                        <p className="count-button"> {this.state.orderAmount} </p>

                        {/*<OverlayTrigger trigger="click" placement="bottom" overlay={<PopOver data={this}/>}>*/}
                        <input type="image"
                               src={plusLogo}
                               className="count-button"
                               alt=""
                               width="20"
                               height="20"
                               onClick={() => this.increaseItem((parseInt(this.props.data.state.food.count)))}
                        />
                        <Button className="disabled mintCreamBGColor food-num-modal">
                            <p className="font grey-color text-center">
                                {(this.props.data.state.food.count - this.state.orderAmount > 0) ? (this.props.data.state.food.count - this.state.orderAmount + ":موجودی") : "ناموجود"}
                            </p>
                        </Button>

                    </div>
                </div>
            </div>
        )
    }
}
