import React, {Component} from 'react';
import './FoodModal.css'
import {Image} from "react-bootstrap";
import starLogo from "../icons/star.svg";
import axios from "axios";
import minusLogo from "../icons/minus.svg";
import plusLogo from "../icons/plus.svg";


export default class FoodModal extends Component {
    foodId;
    restaurantId;

    constructor(props) {
        super(props);
        this.state = {
            foodInfo: {},
            orderAmount: 0,
            hasJWTError: false
        };
        this.closeModal = this.closeModal.bind(this);
    }

    jwtErrorHandling(error){
        console.log(error.response)
        if(error.response && error.response.status === 401)
            alert("کاربر گرامی! برای استفاده از سامانه می‌بایست احراز هویت شوید.بدین منظور درصورت داشتن حساب کاربری وارد سامانه شوید در غیر این صورت ثبت نام کنید.")
        else if(error.response && error.response.status === 403) {
            alert("اشکالی در فرآیند رخ داده است.لطفا مجددا وارد سامانه شوید.")
            localStorage.clear();
        }
        this.setState({hasJWTError:true})
    }

    componentDidUpdate(prevProps) {
        if (this.props.displayModal !== prevProps.displayModal) {
            this.foodId = String(this.props.data.state.modalFood.foodId);
            this.restaurantId = String(this.props.data.state.modalFood.restaurantId);
        }
    }

    addToCart(foodId, restaurantId, orderAmount) {
        console.log(orderAmount)
        axios.post("http://185.166.105.6:31150/LoghmeServlet/cart", null, {
            params: {
                foodId,
                restaurantId,
                orderAmount,
            },
            headers: {Authorization: `Bearer ${localStorage.getItem("token")}`}
        }).then(response => {
            this.setState(prevState => ({
                orderAmount: 0
            }))})
            .catch(error => {
                this.jwtErrorHandling(error)
            });

    }

    increaseItem(){
        this.setState(prevState => ({
            orderAmount: this.state.orderAmount + 1
        }))
    }

    decreaseItem() {
        this.setState(prevState => ({
            orderAmount: (this.state.orderAmount - 1 >= 0)?(this.state.orderAmount - 1):(0)
        }))
    }

    MinusItem = () => (
        <input type="image"
               src={minusLogo}
               alt="Submit"
               width="15"
               height="15"
               onClick={() => this.decreaseItem()}
        />
    );

    PlusItem = () => (
        <input type="image"
               src={plusLogo}
               alt="Submit"
               width="15"
               height="15"
               onClick={() => this.increaseItem()}
        />
    );


    closeModal(e) {
        e.stopPropagation();
        this.props.closeModal()
    }

    render() {
        if(this.state.hasJWTError){
            this.props.history.push({pathname: "/login"});
        }
        const divStyle = {
            display: this.props.displayModal ? 'block' : 'none'
        };
        return (
            <div className="modal" onClick={this.closeModal} style={divStyle}>
                <div className="modal-content" onClick={e => e.stopPropagation()}>
                    <div className="popup-food-box horizontal-centered  white-BG-color border-radius15 shadow">
                        <div
                            className="popup-restaurant-name text-center font-thin font-x-large">{this.props.data.state.modalRestaurantName}</div>
                        <div className="popup-food-info-box row">
                            <div className="col-8">
                                <div className="row">
                                    <div className="col-2 popup-rank">{this.props.data.state.modalFood.popularity}</div>
                                    <div className="col-2 popup-star-pic">
                                        <Image src={starLogo} width="20" height="20" alt=""/>
                                    </div>
                                    <div
                                        className="col-8 popup-food-name font-bold">{this.props.data.state.modalFood.name}</div>
                                </div>
                                <div
                                    className="row popup-description font-thin trolley-grey-color">{this.props.data.state.modalFood.description}</div>
                                <div
                                    className="row popup-price font-thin font-size20">{this.props.data.state.modalFood.price} تومان
                                </div>
                            </div>
                            <div className="col-4 popup-food-pic">
                                <Image src={this.props.data.state.modalFood.image} width="120" height="120" alt=""/>
                            </div>
                        </div>
                        <div className="popup-cart-div col row ">
                            <div className="popup-add-button col-6">
                                <button
                                    className="popup-food-button button medium-turquoise-BG-color border-radius7 black-color font white-color"
                                    type="button"
                                    onClick={() => this.addToCart(this.foodId, this.restaurantId, this.state.orderAmount)}>اضافه
                                    کردن به سبد خرید
                                </button>
                            </div>
                            <div className="popup-inc-add-button col-6 row">
                                <div className="popup-minus-icon col-4">
                                    <this.MinusItem/>
                                </div>
                                <div className="popup-amount col-4">{this.state.orderAmount}</div>
                                <div className="popup-plus-icon col-4">
                                    <this.PlusItem/>
                                </div>
                            </div>

                        </div>
                    </div>
                </div>
            </div>
        )
    }
}
