import React, {Component} from 'react';
import './CartModal.css'
import axios from 'axios';
import minusLogo from "../icons/minus.svg";
import plusLogo from "../icons/plus.svg";
import {withRouter} from 'react-router-dom';
import Spinner from "react-bootstrap/Spinner";


class CartModal extends Component {
    constructor(props) {
        super(props);
        this.state = {
            cart: {},
            cartIsLoaded: false,
            hasJWTError: false
        };

        this.closeModal = this.closeModal.bind(this);
    }

    componentDidUpdate(prevProps) {
        if (this.props.displayModal !== prevProps.displayModal) {
            axios.get('http://185.166.105.6:31150/LoghmeServlet/cart',
                {headers: {Authorization: `Bearer ${localStorage.getItem("token")}`}})
                .then(response => {
                this.setState({cart: response.data, cartIsLoaded: true})
            }).catch(error => {
                this.jwtErrorHandling(error)
            })
        }
    }

    jwtErrorHandling(error){
        console.log(error.response)
        if(error.response && error.response.status === 401)
            alert("کاربر گرامی! برای استفاده از سامانه می‌بایست احراز هویت شوید.بدین منظور درصورت داشتن حساب کاربری وارد سامانه شوید در غیر این صورت ثبت نام کنید.")
        else if(error.response && error.response.status === 403) {
            alert("اشکالی در فرآیند رخ داده است.لطفا مجددا وارد سامانه شوید.")
            localStorage.clear();
        }
        this.setState({hasJWTError:true})
    }

    increaseItem(orderedFoodId, restaurantId){
        axios.post("http://185.166.105.6:31150/LoghmeServlet/cart/single", null, {params:{orderedFoodId, restaurantId},
            headers: {Authorization: `Bearer ${localStorage.getItem("token")}`}})
            .then(response => {
            this.setState({cart: response.data})
        }).catch(error => {
            this.jwtErrorHandling(error)
        })
    }

    decreaseItem(orderedFoodId){
        axios.delete("http://185.166.105.6:31150/LoghmeServlet/cart",{data:{orderedFoodId}, headers: {Authorization: `Bearer ${localStorage.getItem("token")}`}})
            .then(response => {
            this.setState({cart: response.data})
        }).catch(error => {
            this.jwtErrorHandling(error)
        })
    }

    MinusItem = (props) => (
        <input type="image"
               src={minusLogo}
               alt=""
               width="15"
               height="15"
               onClick={() => this.decreaseItem(props.orderedItem.orderedFoodId)}
        />
    );

    PlusItem = (props) => (
        <input type="image"
               src={plusLogo}
               alt=""
               width="15"
               height="15"
               onClick={() => this.increaseItem(props.orderedItem.orderedFoodId, props.restaurantId)}
        />
    );

    finalizeOrder(ordersNum) {
        const path = "/cart/finalizeOrder/"+ordersNum;
        this.props.history.push({pathname:path,ordersNum:ordersNum});
    }


    closeModal(e) {
        e.stopPropagation();
        this.props.closeModal()
    }

    render() {
        if(this.state.hasJWTError){
            this.props.history.push({pathname: "/login"});
        }
        const divStyle = {
            display: this.props.displayModal ? 'block' : 'none'
        };
        return (
            <div className="modal opacity-50" onClick={this.closeModal} style={divStyle}>
                <div className="modal-content" onClick={e => e.stopPropagation()}>
                    <p className="font-bold text-center font-size18 black-color">سبد خرید</p>
                    <div className="cart-border">
                        {this.state.cartIsLoaded && this.props.displayModal && this.state.cart.order && this.state.cart.order.map((order) => (
                            <div>
                                <div className="cart-modal">
                                    <div className="cart-modal-food-name">
                                        <p className="font-thin text-right">{order.name}</p>
                                    </div>

                                    <div className="cart-modal-purchase">
                                        <div className="cart-modal-button">
                                            <this.MinusItem orderedItem={order}/>
                                        </div>
                                        <div className="cart-modal-button"><p className="font-thin">{order.amount}</p></div>
                                        <div className="cart-modal-button">
                                            <this.PlusItem orderedItem={order} restaurantId={this.state.cart.restaurantId}/>
                                        </div>
                                    </div>
                                </div>
                                <div className="cart-modal-price">
                                    <p className="font-thin">{order.price*order.amount} تومان</p>
                                </div>
                                <hr/>
                            </div>
                        ))}
                        {!this.state.cartIsLoaded && <Spinner className="spinner-location" animation="border" role="status">
                            <span className="sr-only">Loading...</span>
                        </Spinner>}
                    </div>
                    <div className="restaurant-menu-total-price font-bold cart-modal-total-price">
                        <p>جمع کل : {this.state.cart.totalPrice} تومان</p>
                    </div>
                    <button
                        className="cart-modal-finalize button border-radius7 font font-bold medium-turquoise-BG-color"
                        type="button" onClick={() => this.finalizeOrder(this.state.cart.ordersNum)}>تایید نهایی
                    </button>
                </div>
            </div>
        )
    }
}

export default withRouter(CartModal);
