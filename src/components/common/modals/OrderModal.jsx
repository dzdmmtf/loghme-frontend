import React, {Component} from 'react';
import Table from "react-bootstrap/Table";
import './OrderModal.css'


export default class FoodPartyModal extends Component {
    constructor(props) {
        super(props);
        this.closeModal = this.closeModal.bind(this);
    }


    closeModal(e) {
        e.stopPropagation();
        this.props.closeModal()
    }

    render() {
        const divStyle = {
            display: this.props.displayModal ? 'block' : 'none'
        };

        return (
            <div className="modal" onClick={this.closeModal} style={divStyle}>
                <div className="modal-content" onClick={e => e.stopPropagation()}>
                    <p className="font font-x-large text-center">{this.props.data.state.modalOrder.restaurantName}</p>
                    <hr className="order-detail-line"/>
                    <Table bordered hovered size="sm">
                        <thead className="table-head">
                            <tr>
                                <th><p className="font grey-color text-center head-text">قیمت</p></th>
                                <th><p className="font grey-color text-center head-text">تعداد</p></th>
                                <th><p className="font grey-color text-center head-text">نام غذا</p></th>
                                <th><p className="font grey-color text-center head-text">ردیف</p></th>
                            </tr>
                        </thead>
                        <tbody>
                        {this.props.data.state.modalOrder.order.map((order, index) => (
                            <tr className="table-row">
                                <td><p className="font text-center row-text">{order.price}</p></td>
                                <td><p className="font text-center row-text">{order.amount}</p></td>
                                <td><p className="font text-center row-text">{order.name}</p></td>
                                <td><p className="font text-center row-text">{index + 1}</p></td>
                            </tr>
                        ))}
                        </tbody>
                    </Table>
                    <div>
                        <p className="font-weight-bold text-left">جمع کل: {this.props.data.state.modalOrder.totalPrice} تومان</p>
                    </div>
                </div>
            </div>
        )
    }

}
