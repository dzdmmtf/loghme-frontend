import React, {Component} from 'react';


export default class Timer extends Component {

    constructor(props) {
        super(props);
        this.state = { time: {}, seconds: 1800 };
        this.timer = 0;
        this.countDown = this.countDown.bind(this);
    }

    secondsToTime(secs){
        let hours = Math.floor(secs / (60 * 60));

        let divisor_for_minutes = secs % (60 * 60);
        let minutes = Math.floor(divisor_for_minutes / 60);

        let divisor_for_seconds = divisor_for_minutes % 60;
        let seconds = Math.ceil(divisor_for_seconds);

        return {
            "h": hours,
            "m": minutes,
            "s": seconds
        };
    }

    componentDidMount() {
        let timeLeftVar = this.secondsToTime(this.state.seconds);
        this.setState({ time: timeLeftVar });
    }

    componentDidUpdate(prevProps) {
        if (this.props.startTimer !== prevProps.startTimer) {
            if (this.props.startTimer) {
                this.timer = setInterval(this.countDown, 1000);
            }
        }
    }

    countDown() {
        let seconds = this.state.seconds - 1;
        this.setState({
            time: this.secondsToTime(seconds),
            seconds: seconds,
        });

        if (seconds === 0) {
            clearInterval(this.timer);
        }
    }

    render() {
        return(
            <div>
                <p className="font-thin time-font text-center">
                    {this.state.time.m}:{this.state.time.s}: زمان باقی‌مانده
                </p>
            </div>
        );
    }
}