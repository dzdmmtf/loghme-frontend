import "./Signup.css"
import "../common/colors.css";
import "../common/fonts/fonts.css";
import "../common/stylesheet.css";
import React, {Component} from "react";
import {Image} from "react-bootstrap";
import registerLogo from "../common/icons/register.svg"
import Footer from "../common/footer/Footer";
import axios from "axios";

export default class Login extends Component {
    constructor(props) {
        super(props);
        this.state = {
            status: "",
            goToHomePage: false
        };
    }

    componentDidMount() {
        axios.get('http://185.166.105.6:31150/LoghmeServlet/jwtChecker',
            {headers: {Authorization: `Bearer ${localStorage.getItem("token")}`}}
        )
            .then(response => {
            }).catch(error => {
            if(error.response.status === 400)
                this.setState({goToHomePage:true})
        })
    }
    getUserInfo() {
        const name = document.getElementById("name").value;
        const familyName = document.getElementById("family-name").value;
        const email = document.getElementById("email").value;
        const phone = document.getElementById("phone").value;
        const password = document.getElementById("password").value;
        const confirmPassword = document.getElementById("confirmPassword").value;
        if (name === "" || familyName === "" || email === "" ||
            phone === "" || password === "" || confirmPassword === "") {
            alert("کاربر گرامی، اطلاعات وارد شده ناقص می‌باشد.")
        } else if (password !== confirmPassword) {
            alert("رمز عبور و تایید رمز عبور یکسان نمی‌باشد.")
        } else {
            axios.post("http://185.166.105.6:31150/LoghmeServlet/signup", {name, familyName, email, phone, password}).then(response => {
                if (response.data.status === "OK") {
                    localStorage.setItem("token", response.data.jwt);
                    this.props.history.push({pathname: "/"});
                } else if (response.data.status === "FORBIDDEN")
                    alert("کاربر گرامی!ایمیل وارد شده قبلا در سامانه ثبت شده است،با این ایمیل قادر به ثبت نام مجدد در لقمه نیستید.")
            })
        }
    }

    SignUpBox = () => (
        <div className="sign-up-main-container mint-cream-BG-color border-radius7 horizontal-centered">
            <div className="sign-up-main-container-header gainsboro-BG-color">
                <Image src={registerLogo} width="27" height="27" alt=""/>
                <p className="font font-size18 horizontal-centered">ثبت نام</p>
            </div>
            <div className="sign-up-main-container-body">
                <input className="sign-up-name horizontal-centered font text-line" type="text" id="name" name="name"
                       placeholder="نام"/><br/><br/>
                <input className="sign-up-family-name horizontal-centered font text-line" type="text" id="family-name"
                       name="family-name"
                       placeholder="نام خانوادگی"/><br/><br/>
                <input className="sign-up-email horizontal-centered font text-line" type="text" id="email" name="email"
                       placeholder="ایمیل"/><br/><br/>
                <input className="sign-up-phone horizontal-centered font text-line" type="text" id="phone" name="phone"
                       placeholder="شماره همراه"/><br/><br/>
                <input className="sign-up-password horizontal-centered font text-line" type="password" id="password"
                       name="password"
                       placeholder="رمزعبور"/><br/><br/>
                <input className="sign-up-confirmPassword font horizontal-centered text-line" type="password"
                       id="confirmPassword" name="confirmPassword" placeholder="تایید رمزعبور"/><br/><br/>
                <button
                    className="sign-up-register-button button font font-size16 horizontal-centered trolley-grey-color gainsboro-BG-color"
                    type="button" onClick={() => this.getUserInfo()}>ثبت نام
                </button>
            </div>
        </div>
    );

    render() {
        if(this.state.goToHomePage){
            this.props.history.push({pathname: "/"});
        }
        return (
            <div className="sign-up">
                <this.SignUpBox/>
                <Footer/>
            </div>
        )
    }
}
