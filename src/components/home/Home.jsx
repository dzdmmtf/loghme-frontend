import React, {Component} from 'react';
import Header from "../common/header/Header";
import "./Home.css"
import "../common/colors.css";
import {Image, Button} from "react-bootstrap";
import logo from "../common/pics/LOGO.png";
import Footer from "../common/footer/Footer";
import star from "../common/icons/003-star.png";
import axios from 'axios';
import FoodPartyModal from "../common/modals/FoodPartyModal";
import Spinner from "react-bootstrap/Spinner";
import Timer from "../common/timer/Timer";


const PhotoBar = () => (
    <div className="top-margin">
        <Header isHomePage={true}/>
        <div className="cover-photo red">
            <Image className="logo-pos" src={logo} width="90" height="90"/>
            <p className="title-pos font-size18 font-weight-bolder white-color">اولین و بزرگ‌ترین وب‌سایت سفارش
                آنلاین غذا در دانشگاه تهران</p>
        </div>
    </div>
);


const SearchBar = props => (
    <div className="search-box">
        <form>
            <input className="search-items font search-button" type="submit" value="جست‌وجو"
                   onClick={props.data.search}/>
            <input className="search-items font" type="text" placeholder="نام رستوران"
                   value={props.data.state.restaurantSearch} onChange={props.data.handleRestaurantSearchChange}/>
            <input className="search-items font" type="text" placeholder="نام غذا" value={props.data.state.foodSearch}
                   onChange={props.data.handleFoodSearchChange}/>
        </form>
    </div>
);


const Title = props => {
    const text = (props.isFoodParty) ? "!جشن غذا" : "رستوران‌ها";
    return (
        <div>
            <p className="english-green-color font-bold font-x-large text-center food-party-text">{text}</p>
            <hr className="food-party-line english-green-color"/>
        </div>
    )
};


const FoodPartyTitle = props => (
    <div>
        <Title isFoodParty={true}/>
        <div className="remaining-time-box">
            <Timer startTimer={props.data.state.startTimer}/>
        </div>
    </div>
);


const FoodParty = props => (
    <div className="food-box-size food-box white-BG-color"
         onClick={() => props.data.handleShowFood(props.restaurantName, props.food, props.restaurantId)}>
        <div className="food-part">
            <div className="food-logo">
                <img className="food-image" src={props.food.image} alt=""/>
            </div>
            <div className="food-info">
                <p className="font text-right food-name">{props.food.name}</p>
                <div className="popularity-part">
                    <p className="font popularity-text text-right">{props.food.popularity}</p>
                    <img className="star-image" src={star} alt=""/>
                </div>
            </div>
        </div>

        <div className="price-part">
            <div className="currentPrice">
                <p className="font price-text">{props.food.price}</p>
            </div>
            <div className="prevPrice">
                <p className="font price-text">{props.food.oldPrice}</p>
            </div>
        </div>

        <div className="purchase-part">
            <div className="purchase-size">
                <Button
                    className={"purchase-elem purchase-button-border " + ((parseInt(props.food.count) > 0) ? "medium-turquoise-BG-color" : "grey-BG-color")}>
                    <p className="font white-color purchase-button-text purchase-text text-center">خرید</p>
                </Button>
            </div>
            <div className="purchase-size">
                <Button className="purchase-elem disabled purchase-num-border  mintCreamBGColor">
                    <p className="font grey-color purchase-button-text purchase-text text-center">
                        {(props.food.count > 0) ? (props.food.count + ":موجودی") : "ناموجود"}
                    </p>
                </Button>
            </div>
        </div>

        <div className="restaurant-part">
            <p className="font font-x text-center restaurant-part-text">{props.restaurantName}</p>
        </div>
    </div>
);


const Restaurants = props => (
    <div className="box restaurant-box-size white-BG-color">
        <div className="restaurant-img">
            <img
                src={props.data.logo}
                className="restaurant-logo"
                alt=""
            />
        </div>
        <p className="font-weight-bold restaurant-name-home restaurant-name-size text-center">{props.data.name}</p>
        <div className="view-menu-container">
            <Button className="view-menu view-menu-size" href={`/restaurants/${props.data.id}/`}>
                <p className="view-menu-text font-weight-bold text-center">نمایش منو</p>
            </Button>
        </div>
    </div>
);


export default class Home extends Component {

    constructor(props) {
        super(props);
        this.state = {
            restaurantSearch: "",
            foodSearch: "",
            searchInfo: [],
            searchIsLoaded: false,
            restaurants: [],
            foodParty: [],
            showFood: false,
            restaurantName: "",
            restaurantId: "",
            food: {},
            foodPartyIsLoaded: false,
            restaurantsIsLoaded: false,
            startTimer: false,
            page: 1,
            count: 5,
            hasJWTError: false
        };

        this.handleRestaurantSearchChange = this.handleRestaurantSearchChange.bind(this);
        this.handleFoodSearchChange = this.handleFoodSearchChange.bind(this);
        this.handleClose = this.handleClose.bind(this);
        this.handleShowFood = this.handleShowFood.bind(this);
        this.search = this.search.bind(this);
        this.showMore = this.showMore.bind(this);
    }


    componentDidMount() {
        axios.post("http://185.166.105.6:31150/LoghmeServlet/restaurants", {
            page: this.state.page,
            count: this.state.count
        },
            {headers: {Authorization: `Bearer ${localStorage.getItem("token")}`}})
            .then(response => {
            this.setState({restaurants: response.data, restaurantsIsLoaded: true});
        }).catch(error => {
            this.jwtErrorHandling(error)
        });

        axios.get("http://185.166.105.6:31150/LoghmeServlet/foodParty",
            {headers: {Authorization: `Bearer ${localStorage.getItem("token")}`}}).then(response => {
            this.setState({foodParty: response.data, foodPartyIsLoaded: true, startTimer: true});
        }).catch(error => {
            this.jwtErrorHandling(error)
        });
    }

    jwtErrorHandling(error){
        console.log(error.respo9556nse)
        this.setState({hasJWTError:true})
    }

    search(event) {
        event.preventDefault();
        if (this.state.restaurantSearch !== "") {
            axios.post("http://185.166.105.6:31150/LoghmeServlet/restaurants/search", {name: this.state.restaurantSearch},
                {headers: {Authorization: `Bearer ${localStorage.getItem("token")}`}}).then(response => {
                this.setState({searchInfo: response.data, searchIsLoaded: true});
            }).catch(error => {
                this.jwtErrorHandling(error)
            })
        } else if (this.state.foodSearch !== "") {
            axios.post("http://185.166.105.6:31150/LoghmeServlet/food/search", {name: this.state.foodSearch},
                {headers: {Authorization: `Bearer ${localStorage.getItem("token")}`}}).then(response => {
                this.setState({searchInfo: response.data, searchIsLoaded: true});
            }).catch(error => {
                this.jwtErrorHandling(error)
            })
        }
    }

    handleRestaurantSearchChange(event) {
        this.setState({restaurantSearch: event.target.value})
    }

    handleFoodSearchChange(event) {
        this.setState({foodSearch: event.target.value})
    }

    handleClose() {
        this.setState({showFood: false});
    }

    handleShowFood(restaurantName, food, restaurantId) {
        this.setState({
            showFood: true,
            restaurantName,
            food,
            restaurantId: restaurantId,
        })
    }

    showMore() {
        axios.post("http://185.166.105.6:31150/LoghmeServlet/restaurants", {page: this.state.page + 1, count: this.state.count},
            {headers: {Authorization: `Bearer ${localStorage.getItem("token")}`}}).then(response => {
            this.setState({
                page: this.state.page + 1,
                restaurants: this.state.restaurants.concat(response.data)
            })
        }).catch(error => {
            this.jwtErrorHandling(error)
        })
    }


    render() {
        if(this.state.hasJWTError){
            this.props.history.push({pathname: "/login"});
        }
        return (
            <div>
                <PhotoBar/>
                <SearchBar data={this}/>
                <FoodPartyTitle data={this}/>
                <div className="food-party-bar">
                    {this.state.foodPartyIsLoaded && this.state.foodParty.map((restaurant, index) => (
                        restaurant.menu.map((food, i) => (
                            <FoodParty data={this} restaurantName={restaurant.name} food={food}
                                       restaurantId={restaurant.id} key={index}/>
                        ))
                    ))}
                    {!this.state.foodPartyIsLoaded &&
                    <Spinner className="spinner-location" animation="border" role="status">
                        <span className="sr-only">Loading...</span>
                    </Spinner>}
                </div>

                <FoodPartyModal displayModal={this.state.showFood} closeModal={this.handleClose} data={this}/>

                <Title isFoodParty={false}/>
                <div className="flex-container">
                    {!this.state.searchIsLoaded && this.state.restaurantsIsLoaded && this.state.restaurants.map((restaurant, index) => (
                        <Restaurants data={restaurant} key={index}/>
                    ))}
                    {this.state.searchIsLoaded && this.state.searchInfo.map((restaurant, index) => (
                        <Restaurants data={restaurant} key={index}/>
                    ))}
                    {!this.state.restaurantsIsLoaded && <Spinner animation="border" role="status">
                        <span className="sr-only">Loading...</span>
                    </Spinner>}
                </div>

                <Button className="show-more-button medium-turquoise-BG-color" onClick={this.showMore}>
                    <p className="font white-color show-more-text text-center">نمایش بیشتر</p>
                </Button>

                <Footer/>
            </div>
        )
    }
}