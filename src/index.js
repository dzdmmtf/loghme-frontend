import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import 'bootstrap/dist/css/bootstrap.min.css';
import * as serviceWorker from './serviceWorker';
import {Switch} from "react-router";
import {BrowserRouter as Router, Route} from 'react-router-dom';
import Home from './components/home/Home';
import Profile from "./components/profile/Profile";
import Login from "./components/login/login";
import RestaurantMenu from "./components/restaurant-menu/RestaurantMenu"
import AddToCart from "./components/cart/AddToCart";
import FinalizeOrder from "./components/cart/FinalizeOrder";
import Signup from "./components/signup/Signup";

ReactDOM.render(
    <Router>
        <Switch>
                <Route exact path="/" component={Home}/>
                <Route exact path="/profile" component={Profile}/>
                <Route exact path="/login" component={Login}/>
                <Route exact path="/signup" component={Signup}/>
                <Route exact path="/restaurants/:restaurantId" component={RestaurantMenu}/>
                <Route exact path="/cart/addToCart/:foodId/:restaurantId" component={AddToCart}/>
                <Route exact path="/cart/finalizeOrder/:ordersNum" component={FinalizeOrder}/>
        </Switch>
    </Router>,
    document.getElementById(`root`)
);

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
serviceWorker.unregister();